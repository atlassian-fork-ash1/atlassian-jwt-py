import pytest
import atlassian_jwt

class Mock(atlassian_jwt.Authenticator):
    def get_shared_secret(self, client_key):
        return 'mysharedsecret'

def test_it_works():
    url = '/path/to/service?zee_last=param&repeated=parameter 1&first=param&repeated=parameter 2'
    token = atlassian_jwt.encode_token(
        'GET', url, clientKey='myclientkey', sharedSecret='mysharedsecret')
    url = url + '&jwt=' + token
    auth = Mock()
    assert auth.authenticate('GET', url) == 'myclientkey'


# start: should raise DecodeError when... -------------------------------------
def test_it_is_missing_jwt_token():
    url = '/path/to/service?zee_last=param&repeated=parameter 1&first=param&repeated=parameter 2'
    auth = Mock()
    with pytest.raises(atlassian_jwt.DecodeError):
        auth.authenticate('GET', url)

def test_it_has_unexpected_qsh_claim():
    url = '/path/to/service?zee_last=param&repeated=parameter 1&first=param&repeated=parameter 2'
    token = atlassian_jwt.encode_token(
        'GET', url, clientKey='myclientkey', sharedSecret='mysharedsecret')
    # remove "repeated=parameter 2" from end of query string
    url = '/path/to/service?zee_last=param&repeated=parameter 1&first=param&jwt=' + token
    auth = Mock()
    with pytest.raises(atlassian_jwt.DecodeError):
        auth.authenticate('GET', url)

def test_it_has_mismatched_shared_secret():
    url = '/path/to/service?zee_last=param&repeated=parameter 1&first=param&repeated=parameter 2'
    token = atlassian_jwt.encode_token(
        'GET', url, clientKey='myclientkey', sharedSecret='mismatch')
    url = url + '&jwt=' + token
    auth = Mock()
    with pytest.raises(atlassian_jwt.DecodeError):
        auth.authenticate('GET', url)
# end: should raise DecodeError when... ---------------------------------------
