"""Test an inbound request from the perspective of the addon. For example, the
addon might include a file view module in its descriptor. This test simulates
the addon servicing a request from the host for that file view during which it
makes an authenticated API call back to the host."""

import atlassian_jwt


class Connection(object):

    def __init__(self, tenant_info):
        self.tenant_info = tenant_info

    def encode(self, http_method, url):
        return atlassian_jwt.encode_token(http_method, url, **self.tenant_info)


class Mock(atlassian_jwt.Authenticator):
    def __init__(self, descriptor):
        super(Mock, self).__init__()
        self.descriptor = descriptor
        self.connections = {}

    def installed(self, tenant_info):
        self.connections[tenant_info['clientKey']] = Connection(tenant_info)

    def encode(self, client_key, http_method, url):
        connection = self.connections[client_key]
        return connection.encode(http_method, url)

    def get_shared_secret(self, client_key):
        connection = self.connections[client_key]
        return connection.tenant_info['sharedSecret']


def test_there_and_back_again():
    descriptor = {
        'key': "myaddonkey",
        'baseUrl': "https://addon",
    }
    addon = Mock(descriptor)
    host = Mock(descriptor)

    # simulate the JSON payload sent to addon's installed lifecycle callback
    tenant_info = {'key': 'myaddonkey',
                   'clientKey': 'myclientkey',
                   'sharedSecret': 'mysharedsecret',
                   'baseUrl': 'https://host/'}
    addon.installed(tenant_info)
    host.installed(tenant_info)

    _simulate_request_to_addon(addon, host, tenant_info)
    _simulate_request_back_to_host(addon, host, tenant_info)


def _simulate_request_to_addon(addon, host, tenant_info):
    # simulate the host encoding the token and sending it as part of the
    # request to the addon for a fileview or other module
    http_method = 'GET'
    url = '/myfileview?repoUuid=x&fileCset=y&filePath=z'
    token = host.encode(tenant_info['clientKey'], http_method, url)
    url = url + '&jwt=' + token

    # simulate the addon authenticating the token
    assert addon.authenticate(http_method, url) == tenant_info['clientKey']


def _simulate_request_back_to_host(addon, host, tenant_info):
    # simulate an API call back to the host
    http_method = 'GET'
    url = '/api/1.0/repositories/{}/x/raw/y/z'
    token = addon.encode(tenant_info['clientKey'], http_method, url)
    headers = {'Authorization': 'JWT {}'.format(token)}

    # simulate the host authenticating the token
    assert (host.authenticate(http_method, url, headers) == 
        tenant_info['clientKey'])
