#! /bin/bash

set -eux

apt-get install -y --no-install-recommends pandoc
pip3.5 install -U pypandoc

cat << EOF > /root/.pypirc
[distutils]
index-servers =
  pypi

[pypi]
repository=https://pypi.python.org/pypi
username=${PYPI_USERNAME}
password=${PYPI_PASSWORD}
EOF

python3.5 setup.py sdist upload -r pypi || true
